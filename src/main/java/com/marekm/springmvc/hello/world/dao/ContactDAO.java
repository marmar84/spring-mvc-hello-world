package com.marekm.springmvc.hello.world.dao;

import java.util.*;

import com.marekm.springmvc.hello.world.model.*;

public interface ContactDAO {

	public void saveContact(Contact contact);

	public List<Contact> getAllContacts();

}
