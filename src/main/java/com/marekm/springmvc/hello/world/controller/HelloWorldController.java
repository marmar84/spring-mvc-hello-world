package com.marekm.springmvc.hello.world.controller;

import java.sql.*;

import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.*;

@Controller
public class HelloWorldController {

	@RequestMapping("/hello1")
	public ModelAndView hello1() {
		String mess = "Hello1 World, current time is: " + new Timestamp(System.currentTimeMillis());
		return new ModelAndView("helloPage", "message", mess);
	}

	@RequestMapping("/hello2")
	@ResponseBody
	public String hello2() {
		return "Hello2 World, current time is: " + new Timestamp(System.currentTimeMillis());
	}
}
