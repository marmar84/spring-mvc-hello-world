package com.marekm.springmvc.hello.world.controller;

import java.util.*;

import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.*;

import com.marekm.springmvc.hello.world.dao.*;
import com.marekm.springmvc.hello.world.model.*;

@Controller
@SessionAttributes
public class ContactController {

	@Autowired
	private ContactDAO contactDAO;

	@RequestMapping(value = "addContact", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("contact") Contact contact) {
		System.out.println("ContactController: addContact: " + contact);
		contactDAO.saveContact(contact);

		return "redirect:contacts";
	}

	@RequestMapping(value = "/contacts")
	public ModelAndView showContats(HttpServletRequest request) {
		System.out.println("ContactController: requestURL: " + request.getRequestURL());
		ModelAndView mav = new ModelAndView("contact", "command", new Contact());
		List<Contact> allContacts = contactDAO.getAllContacts();
		System.out.println("All contacts: " + allContacts);
		mav.addObject("allContacts", allContacts);
		return mav;
	}
}
