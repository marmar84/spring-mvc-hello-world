package com.marekm.springmvc.hello.world.dao;

import java.util.*;

import com.marekm.springmvc.hello.world.model.*;

public class ContactDAOImpl implements ContactDAO {

	private final ContactList contactList;

	public ContactDAOImpl(ContactList contactList) {
		super();
		this.contactList = contactList;
	}

	@Override
	public void saveContact(Contact contact) {
		contactList.add(contact);
		System.out.println("ContactDAOImpl: save contact: " + contact);
	}

	@Override
	public List<Contact> getAllContacts() {
		return contactList.getAll();
	}
}
