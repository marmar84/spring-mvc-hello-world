package com.marekm.springmvc.hello.world.dao;

import java.util.*;

import com.marekm.springmvc.hello.world.model.*;

public class ContactList {
	private final List<Contact> contactsList;

	public ContactList(int initSize) {
		this.contactsList = new ArrayList<Contact>(initSize);
	}

	public void add(Contact contact) {
		contactsList.add(contact);
	}

	public List<Contact> getAll() {
		return contactsList;
	}

}
