<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SpringMvcHelloWorld</title>
<link href="<c:url value='/css/bootstrap.css'/>" rel="stylesheet" media="screen"/>
</head>
<body sytle="margin: 200 px;">

<h3>Strona index.jsp</h3>
<p>
<a href="${pageContext.request.contextPath}/hello1">Say Hello1 - see HelloWorldController</a>
</p>

<p>
<a href="${pageContext.request.contextPath}/hello2">Say Hello2 - see HelloWorldController</a>
</p>


<p>
<a href="${pageContext.request.contextPath}/contacts">Add contact - see ContactController</a>
</p>


<%-- 
<jsp:forward page="contacts"></jsp:forward>
<a href="hello">Say Hello</a>

 --%>
</body>
</html>