<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<title>Spring 3 MVC Series - Contact Manager</title>
<link href="<c:url value='/css/bootstrap.css'/>" rel="stylesheet" media="screen"/>
</head>
<body sytle="margin: 200 px;">
	<h2>Contact Manager</h2>
	<form:form method="post" action="addContact">

		<table>
			<tr>
				<td>
					<form:label path="firstname">First Name</form:label>
				</td>
				<td>
					<form:input path="firstname" />
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="lastname">Last Name</form:label>
				</td>
				<td>
					<form:input path="lastname" />
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="email">Email</form:label>
				</td>
				<td>
					<form:input path="email" />
				</td>
			</tr>
			<tr>
				<td>
					<form:label path="telephone">Telephone</form:label>
				</td>
				<td>
					<form:input path="telephone" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" value="Add Contact" />
				</td>
			</tr>
		</table>

	</form:form>

	<c:if test="${not empty allContacts}">
		<br>
		<br>
		<br>
		<h3>Contact list:</h3>
		<table class="table">
			<tr>
				<th>Lp.</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Telephone</th>
			</tr>
			<c:set var="count" value="0" scope="page" />
			<c:forEach items="${allContacts}" var="element">
			<c:set var="count" value="${count + 1}" scope="page"/>
				<tr>
					<td>${count}</td>
					<td>${element.firstname}</td>
					<td>${element.lastname}</td>
					<td>${element.email}</td>
					<td>${element.telephone}</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>

</body>
</html>